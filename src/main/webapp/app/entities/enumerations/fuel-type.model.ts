export enum FuelType {
  SP98 = 'SP98',
  SP95 = 'SP95',
  GASOIL = 'GASOIL',
  GPL = 'GPL',
  ELECTRIC = 'ELECTRIC',
}
