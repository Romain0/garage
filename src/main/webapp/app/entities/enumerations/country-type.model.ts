export enum CountryType {
  FR = 'FR',

  EN = 'EN',

  ES = 'ES',

  DE = 'DE',

  IT = 'IT',

  UK = 'UK',
}
