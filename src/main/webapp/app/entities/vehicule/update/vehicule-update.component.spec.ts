import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { VehiculeService } from '../service/vehicule.service';
import { IVehicule, Vehicule } from '../vehicule.model';
import { IBrand } from 'app/entities/brand/brand.model';
import { BrandService } from 'app/entities/brand/service/brand.service';
import { ICustomer } from 'app/entities/customer/customer.model';
import { CustomerService } from 'app/entities/customer/service/customer.service';

import { VehiculeUpdateComponent } from './vehicule-update.component';

describe('Vehicule Management Update Component', () => {
  let comp: VehiculeUpdateComponent;
  let fixture: ComponentFixture<VehiculeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let vehiculeService: VehiculeService;
  let brandService: BrandService;
  let customerService: CustomerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [VehiculeUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(VehiculeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(VehiculeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    vehiculeService = TestBed.inject(VehiculeService);
    brandService = TestBed.inject(BrandService);
    customerService = TestBed.inject(CustomerService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Brand query and add missing value', () => {
      const vehicule: IVehicule = { id: 456 };
      const brand: IBrand = { id: 72899 };
      vehicule.brand = brand;

      const brandCollection: IBrand[] = [{ id: 65570 }];
      jest.spyOn(brandService, 'query').mockReturnValue(of(new HttpResponse({ body: brandCollection })));
      const additionalBrands = [brand];
      const expectedCollection: IBrand[] = [...additionalBrands, ...brandCollection];
      jest.spyOn(brandService, 'addBrandToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ vehicule });
      comp.ngOnInit();

      expect(brandService.query).toHaveBeenCalled();
      expect(brandService.addBrandToCollectionIfMissing).toHaveBeenCalledWith(brandCollection, ...additionalBrands);
      expect(comp.brandsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Customer query and add missing value', () => {
      const vehicule: IVehicule = { id: 456 };
      const customer: ICustomer = { id: 42092 };
      vehicule.customer = customer;

      const customerCollection: ICustomer[] = [{ id: 85105 }];
      jest.spyOn(customerService, 'query').mockReturnValue(of(new HttpResponse({ body: customerCollection })));
      const additionalCustomers = [customer];
      const expectedCollection: ICustomer[] = [...additionalCustomers, ...customerCollection];
      jest.spyOn(customerService, 'addCustomerToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ vehicule });
      comp.ngOnInit();

      expect(customerService.query).toHaveBeenCalled();
      expect(customerService.addCustomerToCollectionIfMissing).toHaveBeenCalledWith(customerCollection, ...additionalCustomers);
      expect(comp.customersSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const vehicule: IVehicule = { id: 456 };
      const brand: IBrand = { id: 14096 };
      vehicule.brand = brand;
      const customer: ICustomer = { id: 51385 };
      vehicule.customer = customer;

      activatedRoute.data = of({ vehicule });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(vehicule));
      expect(comp.brandsSharedCollection).toContain(brand);
      expect(comp.customersSharedCollection).toContain(customer);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Vehicule>>();
      const vehicule = { id: 123 };
      jest.spyOn(vehiculeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ vehicule });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: vehicule }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(vehiculeService.update).toHaveBeenCalledWith(vehicule);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Vehicule>>();
      const vehicule = new Vehicule();
      jest.spyOn(vehiculeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ vehicule });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: vehicule }));
      saveSubject.complete();

      // THEN
      expect(vehiculeService.create).toHaveBeenCalledWith(vehicule);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Vehicule>>();
      const vehicule = { id: 123 };
      jest.spyOn(vehiculeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ vehicule });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(vehiculeService.update).toHaveBeenCalledWith(vehicule);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackBrandById', () => {
      it('Should return tracked Brand primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackBrandById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCustomerById', () => {
      it('Should return tracked Customer primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCustomerById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
