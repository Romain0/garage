import { IAddress } from 'app/entities/address/address.model';
import { IVehicule } from 'app/entities/vehicule/vehicule.model';

export interface ICustomer {
  id?: number;
  firstName?: string | null;
  lastName?: string | null;
  email?: string | null;
  telephone?: string | null;
  address?: IAddress | null;
  vehicules?: IVehicule[] | null;
}

export class Customer implements ICustomer {
  constructor(
    public id?: number,
    public firstName?: string | null,
    public lastName?: string | null,
    public email?: string | null,
    public telephone?: string | null,
    public address?: IAddress | null,
    public vehicules?: IVehicule[] | null
  ) {}
}

export function getCustomerIdentifier(customer: ICustomer): number | undefined {
  return customer.id;
}
