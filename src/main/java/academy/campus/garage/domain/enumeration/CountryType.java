package academy.campus.garage.domain.enumeration;

/**
 * The CountryType enumeration.
 */
public enum CountryType {
    FR,
    EN,
    ES,
    DE,
    IT,
    UK,
}
