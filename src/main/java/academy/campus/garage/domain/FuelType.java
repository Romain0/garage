package academy.campus.garage.domain;

public enum FuelType {
    SP98, SP95, GASOIL, GPL, ELECTRIC
}
