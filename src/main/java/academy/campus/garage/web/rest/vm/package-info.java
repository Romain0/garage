/**
 * View Models used by Spring MVC REST controllers.
 */
package academy.campus.garage.web.rest.vm;
