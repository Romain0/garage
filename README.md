# garage

This application was generated using JHipster 7.8.1, you can find documentation and help at [https://www.jhipster.tech](https://www.jhipster.tech).

## Commandes

### Projet

- `./npmw install` - lance locallement ` npm install`
- `./mnvw` - lance le projet maven

### Opérations

- `jhipster entity <Entite> --regenerate` - Met à jour l'entité
